# Rapport - slide 1
> Author: CAI Pengfei

> Note: if you don't know how to configure environement in linux, please refer to installguide.md
## Question 1
**Add a screencopy of this page to your report, with the URL:**

Response:

<img src="./rapport/figs/page_init.png" alt="screenshot" style="display:block; max-width:500px;">

**Do you have any remark about the files?**

Compare with an usual J2EE project.
- Are you sure the files are at the same place?

- What about the web.xml file?

Response:
- For Spring project jsp files are moved to jsp folder.  And they belong to WEB-INF folder right now.

- There are three more xml files: `applicationContext.xml`, `dispatcher-servlet.xml` and `web.xml`.

- There are two more xml files in Configuration files:
`web-fragment.xml` and `web.xml`

- For Spring project, `web.xml` defines Servlet annotation, we need to add servlets in this file and avoid creating servlets with annotations.

## Question 2
**Explain the elements of the HelloController
source code according to the explanations
of the previous slide: for each point
describe which instructions are used.**

Response:
- `@Controller` is an annotation that tells it is a Controller.

- `public HelloContorller(){}` is a constructor of the controller.

- `@RequestMapping(method=RequestMethod.GET)` is an annotation that tells we handle the GET method.

- Return type `ModelAndView` defines what we display.

**What are HttpServletRequest and
HttpServletResponse?**

Response:
- `HttpServletRequest` extends the `ServletRequest` interface to provide request information for HTTP servlets.

- `HttpServletResponse` extends the `ServletResponse` interface to provide HTTP-specific functionality in sending a response.

## Question 3
**Add a screencopy of your page to your report.
Do not forget to give the URL you used.**

Response:
- URL: http://localhost:8080/prwebSpr/hello.htm
<img src="./rapport/figs/hello_htm.png" alt="screenshot" style="display:block; max-width:500px;">

**Change configuration files so that we can reach the hello
controller when calling welcome.htm
Add a screencopy of this page to your report, with the URL.
You must not change any file name.**

Response:
- We only need to change attribute `key` from `hello.htm` to `welcome.htm` of urlMapping prop in `dispatcher-servlet.xml`

- URL: http://localhost:8080/prwebSpr/welcome.htm
<img src="./rapport/figs/welcome_htm.png" alt="screenshot" style="display:block; max-width:500px;">

**Why do we use hello instead of hello.jsp in ModelAndView definition?**

Response:
- In the definition of `ModelAndView` constructor `public ModelAndView(java.lang.String viewName)`, viewName is a name of the view to render and to be resolved by `dispatcher-servlet.xml`. And the bean `viewResolver` of the xml has indiced that the view will be suffixed by `p:suffix`(`.jsp`) string

**How could we change the jsp locations?**

Response:
- We only need to change `p:prefix` string in bean `viewResolver`

## Question 4
**Change the name of the method handleHello in the controller to handleGET. Relaunch your application and call the page in your browser.
It still works. Why?**

Response:
- It still works. SPRING uses design patterns to work, we can use the method name we want. The import element is the **annotation**.

**Remove the parameters from the method in the controller. Relaunch your application and call the page in your browser. It still works. Why?**

Response:
- we can use other parameters, The import element is the **annotation**.

## Question 5
**Ending with .htm is not cool. We want to end calls with `.do`!
Modify configura2on files so that we can use `.do` instead of `.htm`**

**Have a look at dispatcher-servlet.xml, web.xml and redirect.jsp for the modifications. You may change something in these files.**

**Maybe you will have to add/change the default page in web.xmlor in redirect.jsp. Explain in your report the modifica2ons you did.**

Response:
- Firstly, we need to modify url pattern from `*.htm` to `*.do` in `web.xml` to make URL which ends with `.do` can be send to dispatcher servlet.

- Then we need to modify attribute `key` from `index.htm` to `index.do` and `hello.htm` to `hello.do` of urlMapping prop in `dispatcher-servlet.xml`.

- Finally we need to modify `redirect.jsp` to make it `sendRedirect("hello.do")` in that our welcome page will be `hello.do`.

## Question 6
**Test the 2 methods to add parameters.
Add a screencopy of each page to your report.
Is the result the same one?**

Response:
- The results are same.
<img src="./rapport/figs/jstl_test.png" alt="screenshot" style="display:block; max-width:500px;">

**In the 2 possibili2es, how can you change FirstName to
firstName? Which modifica2ons would you have to do?**

Response:
- If we use HashMap, we need to change the key from `FirstName` to `firstName`. We also need to change `hello.jsp`, `${FirstName}`->`${firstName}`.

- If we use addObject, we need to change the first parameter from `FirstName` to `firstName`. We also need to change `hello.jsp`, `${FirstName}`->`${firstName}`.

## Question 7
**Add a screencopy of this page to your report,
with your first name and last name.**

Response:
- screenshot:
<img src="./rapport/figs/jstl_liste.png" alt="screenshot" style="display:block; max-width:500px;">

**Change ArrayList to LinkedList
Change the values
Replace the List of String by a List of Object and change
value "item 2" to 123**

**Add a screencopy of the result pages to your report**

Response:
- screenshot:
<img src="./rapport/figs/jstl_liste2.png" alt="screenshot" style="display:block; max-width:500px;">


## Question 8
**Add a screencopy of your result.**

**Change akribute passw to password in User.java
Test it. It should continue to work without changing the jsp file.**

**Change getPassw() in User.java to getPassword() Test it. Does it s2ll work?
What is your conclusion?**

Response:
- Sreenshot:
<img src="./rapport/figs/jstl_object.png" alt="screenshot" style="display:block; max-width:500px;">

- If I only change attribute, it still work without changing jsp file.

- If change getters in `User.java`, it will raise an Internal Server Error:
<img src="./rapport/figs/jstl_object_error.png" alt="screenshot" style="display:block; max-width:500px;">

- Conclusion: the attributes of jstl object in fact calls getter of a Java class.

## Question 9
**Test object access with arrays.
Add a screencopy of the result to your report.
Explain what happens.**

Response:
- Screenshot:
<img src="./rapport/figs/jstl_object_array.png" alt="screenshot" style="display:block; max-width:500px;">

- It also calls getter of java class. So it's same with the former question.

# Rapport - slide 2

## Question 1
**In Item.java and Category.Java you will find annotations.
List them, and explain what they mean.**

Response:
- `@Entity` means it's an Entity class. Each entity class usually represents a table in a relational database. Each instance of Entity class corresponds to a row in a table.

- `@Table(name = "category")` means this Entity class represents table `category` in relational database.

- `@XmlRootElement`, When a top level class or an enum type is annotated with the `@XmlRootElement` annotation, then its value is represented as XML element in an XML document.

- `@NamedQueries` specifies multiple named Java Persistence query language queries.,`NamedQuery` specifies a static, named query in the Java Persistence query language.

- `@Id` specifies the primary key property or field of an entity.

- `@GeneratedValue` allows you to specify the strategy that  automatically generates the values of primary keys. Used with `@Id`. `@GeneratedValue(strategy = GenerationType.IDENTITY)` specify the strategy is `GenerationType.IDENTITY`

- `@Basic` means use the simplest type of mapping to a database column.

- `@Column` specifies a mapped column for a persistent property or field.

- `@OneToMany` defines a many-valued association with one-to-many multiplicity.

- `@ManyToOne` defines a single-valued association to another entity class that has many-to-one multiplicity.

- `@ManyToMany` defines a many-valued association with many-to-many multiplicity.

- `@XmlTransient` prevents the mapping of a JavaBean property/type to XML representation. And it is useful for resolving name collisions between a JavaBean property name and a field name or preventing the mapping of a field/property.

**What do you find in the persistence.xml file?
What is the name of the persistence-unit?
Which classes persist?**

Response:
- In `persistence.xml`, we can find persistence unit name, the Entity class that we include and the information of  database connection.

- name of persistence-unit: `prwebSprPU`.

- Two classes persit: `org.centrale.prweb.items.Item` and `org.centrale.prweb.items.Category`


## Question 2
**The red bracket in slide 28 indicated the lines that implement the Design Pattern: Singleton.**

**In these lines, you may replace MySpringAppPU by your persistence unit name – defined in
persistence.xml**


**We manage 1 and only 1 JPA environment. Why?**

Response:
- We use `Persistence.createEntityManagerFactory` to create an EntityManagerFactory for the named persistence unit. So that we manage 1 JPA environment which we named in `persistence.xml`

**Why do we need the EntityManagerFactory to be
unique?**

Response:
- Persistence unit is a logical grouping that contains information like configuration of EntityManagerFactory. Each persistence unit must have a unique name. An application can have one or more persistence units. For this project, the item manager is set to manage item, it corresponds to the persistence unit that we named in `persistence.xml`, so it need to be unique.

**How could we build a query and ensure that the result is unique?**

Response:
- We use `createNamedQuery(java.lang.String name, java.lang.Class<T> resultClass)` to build a query. the use `resultClass` to indicate the type of query result.

## Question 3
**Add a screencopy of this page to your report, with the URL**

Response:
- screenshot
<img src="./rapport/figs/items.png" alt="screenshot" style="display:block; max-width:500px;">

- URL: http://localhost:8080/prwebSpr/listitems.do

**When we created the EntityManagerFactory in ItemManagerImpl, we give a parameter. Where does it come from?**

Response:
- The parameter is the name of persistent unit that we named in `persistence.xml`

**In the page() method, we use a named query, where is it defined?**

Response:
- It was defined in annotation `@NamedQuery` of  `Item` class.

**When we display the Item, in the JSP file, we access id, title, etc. How do we access them? Which other way of accessing these attributes could you use?**

Response:
- we access them as arrays

- We can also acess them as attributes like:
`item.id`, `item.title`, etc.

# Rapport - slide 3


## Question 1
**Add a screencopy of the 2 pages to your
report, with the URL**

Response:
- URL: http://localhost:8080/prwebSpr/index.do
<img src="./rapport/figs/login.png" alt="screenshot" style="display:block; max-width:500px;">

- URL: http://localhost:8080/prwebSpr/listitems.do
<img src="./rapport/figs/login_item.png" alt="screenshot" style="display:block; max-width:500px;">

**The method that handles POST does not have the same parameters than the one that Handles GET but it still works. Why?**

Response:
- Because SPRING use Design Pattern, the import thing is annotation, we can use other function name and other number of parameters that we want.

**What is the ModelAttribute annotation used for?**

Response:
- `@ModelAttribute` is an annotation that binds a method parameter or method return value to a named model attribute. For our project, it has binds `login` and `pwd` in jsp file to `User` model attributes.

## Question 2
**Add screencopies of the result to your report
What is the URL called when you click on the button in itemAdd.jsp? Is it called with method GET or POST?**

Response:
- URL: http://localhost:8080/prwebSpr/index.do
<img src="./rapport/figs/login.png" alt="screenshot" style="display:block; max-width:500px;">

- URL: http://localhost:8080/prwebSpr/listitems.do
<img src="./rapport/figs/login_item.png" alt="screenshot" style="display:block; max-width:500px;">

- URL: http://localhost:8080/prwebSpr/itemsAdd.do
<img src="./rapport/figs/item_add.png" alt="screenshot" style="display:block; max-width:500px;">

- When I click the button in `itemAdd.jsp`, URL: http://localhost:8080/prwebSpr/itemCreate.do will be called and with method `POST`.

**Which route did you define in dispatcher-servlet.xml? Which controller do you call?**

Response:
- `<prop key="itemsAdd.do">itemAddController</prop>`

- I call the `itemAddController`

**Which view is called in ItemAddController.java with the GET request? With the POST request?**

Response:
- `index.jsp` will be called with the GET request.

- `itemAdd.jsp` will be called with the POST method.

## Question 3
**Insert a new item, with you as a seller.
Add a screencopy of the pages to your
report, with the URL.**

- **List before adding an item**

- **Adding an item**

- **List after adding an item**

Response:
- Before add a new Item - URL: http://localhost:8080/prwebSpr/listitems.do
<img src="./rapport/figs/login_item.png" alt="screenshot" style="display:block; max-width:500px;">

- adding an item - URL: http://localhost:8080/prwebSpr/itemsAdd.do
<img src="./rapport/figs/item_adding.png" alt="screenshot" style="display:block; max-width:500px;">

- After add a new Item - URL: http://localhost:8080/prwebSpr/index.do
<img src="./rapport/figs/item_added.png" alt="screenshot" style="display:block; max-width:500px;">

**Why did we use the EntityManager to save
the item instead of a SQL request?**

Response:
- Certain projects require engineers to focus more on the object model rather than on the actual SQL queries used to access data stores. With JPA (implementations) or most other ORM frameworks, it is possible to have all entities i.e. tables in your database, modelled as classes in Java. Additionally, it also possible to embed behavior into these classes and therefore achieve a behaviorally rich domain model.

# Rapport -slide 4


## Question 1
**Add a screencopy of your page to your report, with the URL**

Response:
- URL: http://localhost:8080/prwebSpr/listitems.do
<img src="./rapport/figs/item_category.png" alt="screenshot" style="display:block; max-width:500px;">

**Explain the meaning of ${item['categoryId']['name']}
Which syntax could you use for the same result?**

Response:
- we access item as arrays

- We can also acess it as attributes like:
`item.categoryId.name`

## Question 2
**Try your program without changing anything else to the ItemManager, ItemManagerImpl and Item classes. Add a copy of the inserted item page to your report**

**What is the result? Explain it.**

Response:
- When adding an item. URL: http://localhost:8080/prwebSpr/itemsAdd.do
<img src="./rapport/figs/item_adding_category.png" alt="screenshot" style="display:block; max-width:500px;">

- After adding an item, URL: http://localhost:8080/prwebSpr/itemCreate.do
<img src="./rapport/figs/item_added_category.png" alt="screenshot" style="display:block; max-width:500px;">

- Explaination: we obtain the list of possible categories by using persistence query `category.findAll` and transfer the result list to Object `listCategory`. Then we display the `listCategory` by using element `<select></select>` and jstl. After we've populated the line which need to be added, the result will be parsed to a class Item by using annotation `@ModelAttribute`. Then we commit it to database and we call `item.jsp` to display all rows of table item.
