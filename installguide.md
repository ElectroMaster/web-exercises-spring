## 0. Java install
download oracle java jdk 8
```
sudo mkdir /usr/lib/jvm
sudo tar zxvf jdk-8u201-linux-x64.tar.gz -C /usr/lib/jvm
```
add java to environement
```
sudo vim ~/.bashrc
```
and add these lines:
```
#set oracle jdk environment
export JAVA_HOME=/usr/lib/jvm/jdk1.8.0_14 ## 这里要注意目录要换成自己解压的jdk 目录
export JRE_HOME=${JAVA_HOME}/jre
export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib
export PATH=${JAVA_HOME}/bin:$PATH

```
last step: `source ~/.bashrc`
### check installation
tab `java -version` and you can see java information

## 1. Install http server: apache2
```
sudo apt install apache2
sudo service apache2 start
```
apache2 config file locate at`/etc/apache2/apache2.conf`, you can changeit based on your needs
apache file storage location is `/var/www/html`

## 2. Install postgresql on ubuntu
create file `/etc/apt/sources.list.d/pgdg.list` and a line for the respository.
```
deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main
```
import the repository signing key, and update the package lists
```
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
```
installation
```
sudo apt install postgresql-10
```
if pgAdmin4 not installed
```
sudo apt install pgadmin4
```

### configuration for postgres
```
sudo -u postgres psql
```
OR
```
# switch to user postgres
sudo -i -u postgres
# create user
createuser --interactive
# then shell will ask you user name and superuser or not

# login psql
psql
```
change passwd of database `postgres` to `123456`, you can do it for your created user.
```
postgres=# ALTER USER postgres WITH PASSWORD '123456';
```
quit psql
```
postgres=# \q
```

change PostgreSQL server passwd for user `postgres`
```
sudo -u postgres passwd
```
you add UNIX password for user `postgres`, here I set `123456`

### Remote connection
cite: https://blog.csdn.net/mashuai720/article/details/79413571

### pgadmin connection
```
sudo apt install pgadmin4
```

tab `pgadmin4` in ubuntu shell and wait for the web page.

In the web page, right click on `server` and set connection name, database address etc.

## 3. Install SQL Power Architecture
cite web: http://www.bestofbi.com/page/architect_download_os

## 4. Install Apache Tomcat
- download binary tomcat file from http://tomcat.apache.org
- unzip: `tar -zxvf apache-tomcat-9.0.14.tar.gz`
- change contenue of `$tomcat_dir$/bin/startup.sh` **note**: version after 9.0.16 seems to have fixed this bug.
- add these lines in the head
-
```
JAVA_HOME=/usr/lib/jvm/java-8-oracle
JRE_HOME=/usr/lib/jvm/java-8-oracle/jre
CLASS_PATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib
PATH=$JAVA_HOME/bin:$JRE_HOME/bin:$PATH
TOMCAT_HOME=/opt/apache-tomcat-9.0.14
```

if you can see
```
Using CATALINA_BASE: /opt/apache-tomcat-8.0.12Using 

CATALINA_HOME:  /opt/apache-tomcat-8.0.12Using 

CATALINA_TMPDIR:  /opt/apache-tomcat-8.0.12/tempUsing 

JRE_HOME:   /usr/java/jdk1.8.0_20Using 

CLASSPATH: /opt/apache-tomcat-8.0.12/bin/bootstrap.jar:/opt/apache-tomcat-8.0.12/bin/tomcat-juli.jarTomcat started.
```
then it has been installed successfully

### check installation
tap `http://localhost:8080/` in your browser.

### start tomcat
```
sudo .$tomcat_dir$/bin/startup.sh
```
### stop tomcat
```
sudo $tomcat_dir$/bin/shutdown.sh
```

## 5. Install PHP
```
sudo apt install php7.2 libapache2-mod-php7.2 php7.2-mysql php7.2-curl php7.2-json php7.2-gd
sudo apt install php-msgpack php-memcached php7.2-intl php7.2-sqlite3 php7.2-gmp php-geoip php7.2-mbstring php7.2-xml php7.2-zip php7.2-pgsql
```
**On ubuntu 18.04 LTS you will have to replace 7 by 7.2**

### Configure php for http server
```
cd /etc/apache2
```
create sympol link to /usr/lib/apache2/modules
```
sudo ln -s /usr/lib/apache2/modules/ modules
```
create `httpd.conf` in `/etc/apache2` folder and tape:
```
LoadModule php7.2_module modules/libphp7.2.so
```
Then relaunch http server
```
service apache2 restart
```

### Check it works
Create file `infos.php` in `/var/www/html` and add these content follow:
```php
<?php
phpinfo();
print "<hr/>\n";
print 'register_globals = ' . ini_get('register_globals') . "\n";
?>
```
In your browser open http://127.0.0.1/infos.php and you will have php infos, check pdo-pgsql is ok

## 6. Install doxygen
First download binary file from http://www.doxygen.org
Then extract file.
To avoid an error you need to download `doxygentag` from [stackflow](https://stackoverflow.com/questions/15020691/installation-of-doxygen-and-error-in-make-command). and put it in `<doxygen_root>/bin` folder.
```
./configure
sudo make insttall
```
if it show `there is not folder examples` you need to mv `examples` foler from `<doxygen_root>/html` foler to `<doxygen_root>`.

## 7. Install Jquery file
Download it from http://jquery.com/download/
• Get compressed 3.3.1 file
• Place file "jquery-3.3.1.min.js in `/var/www` folder.


## when you want to connect postgresql server using PDO
```
cd /etc/php/7.2/apache2
sudo gedit php.ini
```
change `;extension=pdo_pgsql` to `extension=pdo_pgsql`;

## Install symfony
```
curl -sS https://get.symfony.com/cli/installer | bash
```
## Install composer
```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
php -r "unlink('composer-setup.php');"
```
### Setting up the symfony framework
```
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
# install the web server bundle, first time
 cd my-project
 composer require symfony/web-server-bundle --dev
 # if already installed
 composer require server --dev
```

### runing your symfony application
```
cd my-project
php bin/console server:run
```


## Netbeans ugly fonts
```
vim <netbeans-root>/etc/netbeans.conf
```
add `-J-Dswing.aatext=true -J-Dawt.useSystemAAFontSettings=on` at the end of netbeans default options.
