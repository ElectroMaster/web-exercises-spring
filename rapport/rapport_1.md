# Rapport - slide 1
> Author: CAI Pengfei

## Question 1
**Add a screencopy of this page to your report, with the URL:**

Response:

<img src="./figs/page_init.png" alt="screenshot" style="display:block; max-width:500px;">

**Do you have any remark about the files?**

Compare with an usual J2EE project.
- Are you sure the files are at the same place?

- What about the web.xml file?

Response:
- For Spring project jsp files are moved to jsp folder.  And they belong to WEB-INF folder right now.

- There are three more xml files: `applicationContext.xml`, `dispatcher-servlet.xml` and `web.xml`.

- There are two more xml files in Configuration files:
`web-fragment.xml` and `web.xml`

- For Spring project, `web.xml` defines Servlet annotation, we need to add servlets in this file and avoid creating servlets with annotations.

## Question 2
**Explain the elements of the HelloController
source code according to the explanations
of the previous slide: for each point
describe which instructions are used.**

Response:
- `@Controller` is an annotation that tells it is a Controller.

- `public HelloContorller(){}` is a constructor of the controller.

- `@RequestMapping(method=RequestMethod.GET)` is an annotation that tells we handle the GET method.

- Return type `ModelAndView` defines what we display.

**What are HttpServletRequest and
HttpServletResponse?**

Response:
- `HttpServletRequest` extends the `ServletRequest` interface to provide request information for HTTP servlets.

- `HttpServletResponse` extends the `ServletResponse` interface to provide HTTP-specific functionality in sending a response.

## Question 3
**Add a screencopy of your page to your report.
Do not forget to give the URL you used.**

Response:
- URL: http://localhost:8080/prwebSpr/hello.htm
<img src="./figs/hello_htm.png" alt="screenshot" style="display:block; max-width:500px;">

**Change configuration files so that we can reach the hello
controller when calling welcome.htm
Add a screencopy of this page to your report, with the URL.
You must not change any file name.**

Response:
- We only need to change attribute `key` from `hello.htm` to `welcome.htm` of urlMapping prop in `dispatcher-servlet.xml`

- URL: http://localhost:8080/prwebSpr/welcome.htm
<img src="./figs/welcome_htm.png" alt="screenshot" style="display:block; max-width:500px;">

**Why do we use hello instead of hello.jsp in ModelAndView definition?**

Response:
- In the definition of `ModelAndView` constructor `public ModelAndView(java.lang.String viewName)`, viewName is a name of the view to render and to be resolved by `dispatcher-servlet.xml`. And the bean `viewResolver` of the xml has indiced that the view will be suffixed by `p:suffix`(`.jsp`) string

**How could we change the jsp locations?**

Response:
- We only need to change `p:prefix` string in bean `viewResolver`

## Question 4
**Change the name of the method handleHello in the controller to handleGET. Relaunch your application and call the page in your browser.
It still works. Why?**

Response:
- It still works. SPRING uses design patterns to work, we can use the method name we want. The import element is the **annotation**.

**Remove the parameters from the method in the controller. Relaunch your application and call the page in your browser. It still works. Why?**

Response:
- we can use other parameters, The import element is the **annotation**.

## Question 5
**Ending with .htm is not cool. We want to end calls with `.do`!
Modify configura2on files so that we can use `.do` instead of `.htm`**

**Have a look at dispatcher-servlet.xml, web.xml and redirect.jsp for the modifications. You may change something in these files.**

**Maybe you will have to add/change the default page in web.xmlor in redirect.jsp. Explain in your report the modifica2ons you did.**

Response:
- Firstly, we need to modify url pattern from `*.htm` to `*.do` in `web.xml` to make URL which ends with `.do` can be send to dispatcher servlet.

- Then we need to modify attribute `key` from `index.htm` to `index.do` and `hello.htm` to `hello.do` of urlMapping prop in `dispatcher-servlet.xml`.

- Finally we need to modify `redirect.jsp` to make it `sendRedirect("hello.do")` in that our welcome page will be `hello.do`.

## Question 6
**Test the 2 methods to add parameters.
Add a screencopy of each page to your report.
Is the result the same one?**

Response:
- The results are same.
<img src="./figs/jstl_test.png" alt="screenshot" style="display:block; max-width:500px;">

**In the 2 possibili2es, how can you change FirstName to
firstName? Which modifica2ons would you have to do?**

Response:
- If we use HashMap, we need to change the key from `FirstName` to `firstName`. We also need to change `hello.jsp`, `${FirstName}`->`${firstName}`.

- If we use addObject, we need to change the first parameter from `FirstName` to `firstName`. We also need to change `hello.jsp`, `${FirstName}`->`${firstName}`.

## Question 7
**Add a screencopy of this page to your report,
with your first name and last name.**

Response:
- screenshot:
<img src="./figs/jstl_liste.png" alt="screenshot" style="display:block; max-width:500px;">

**Change ArrayList to LinkedList
Change the values
Replace the List of String by a List of Object and change
value "item 2" to 123**

**Add a screencopy of the result pages to your report**

Response:
- screenshot:
<img src="./figs/jstl_liste2.png" alt="screenshot" style="display:block; max-width:500px;">


## Question 8
**Add a screencopy of your result.**

**Change akribute passw to password in User.java
Test it. It should continue to work without changing the jsp file.**

**Change getPassw() in User.java to getPassword() Test it. Does it s2ll work?
What is your conclusion?**

Response:
- Sreenshot:
<img src="./figs/jstl_object.png" alt="screenshot" style="display:block; max-width:500px;">

- If I only change attribute, it still work without changing jsp file.

- If change getters in `User.java`, it will raise an Internal Server Error:
<img src="./figs/jstl_object_error.png" alt="screenshot" style="display:block; max-width:500px;">

- Conclusion: the attributes of jstl object in fact calls getter of a Java class.

## Question 9
**Test object access with arrays.
Add a screencopy of the result to your report.
Explain what happens.**

Response:
- Screenshot:
<img src="./figs/jstl_object_array.png" alt="screenshot" style="display:block; max-width:500px;">

- It also calls getter of java class. So it's same with the former question.
