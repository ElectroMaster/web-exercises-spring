# Rapport - slide 2
> Author: CAI Pengfei
## Question 1
**In Item.java and Category.Java you will find annotations.
List them, and explain what they mean.**

Response:
- `@Entity` means it's an Entity class. Each entity class usually represents a table in a relational database. Each instance of Entity class corresponds to a row in a table.

- `@Table(name = "category")` means this Entity class represents table `category` in relational database.

- `@XmlRootElement`, When a top level class or an enum type is annotated with the `@XmlRootElement` annotation, then its value is represented as XML element in an XML document.

- `@NamedQueries` specifies multiple named Java Persistence query language queries.,`NamedQuery` specifies a static, named query in the Java Persistence query language.

- `@Id` specifies the primary key property or field of an entity.

- `@GeneratedValue` allows you to specify the strategy that  automatically generates the values of primary keys. Used with `@Id`. `@GeneratedValue(strategy = GenerationType.IDENTITY)` specify the strategy is `GenerationType.IDENTITY`

- `@Basic` means use the simplest type of mapping to a database column.

- `@Column` specifies a mapped column for a persistent property or field.

- `@OneToMany` defines a many-valued association with one-to-many multiplicity.

- `@ManyToOne` defines a single-valued association to another entity class that has many-to-one multiplicity.

- `@ManyToMany` defines a many-valued association with many-to-many multiplicity.

- `@XmlTransient` prevents the mapping of a JavaBean property/type to XML representation. And it is useful for resolving name collisions between a JavaBean property name and a field name or preventing the mapping of a field/property.

**What do you find in the persistence.xml file?
What is the name of the persistence-unit?
Which classes persist?**

Response:
- In `persistence.xml`, we can find persistence unit name, the Entity class that we include and the information of  database connection.

- name of persistence-unit: `prwebSprPU`.

- Two classes persit: `org.centrale.prweb.items.Item` and `org.centrale.prweb.items.Category`


## Question 2
**The red bracket in slide 28 indicated the lines that implement the Design Pattern: Singleton.**

**In these lines, you may replace MySpringAppPU by your persistence unit name – defined in
persistence.xml**


**We manage 1 and only 1 JPA environment. Why?**

Response:
- We use `Persistence.createEntityManagerFactory` to create an EntityManagerFactory for the named persistence unit. So that we manage 1 JPA environment which we named in `persistence.xml`

**Why do we need the EntityManagerFactory to be
unique?**

Response:
- Persistence unit is a logical grouping that contains information like configuration of EntityManagerFactory. Each persistence unit must have a unique name. An application can have one or more persistence units. For this project, the item manager is set to manage item, it corresponds to the persistence unit that we named in `persistence.xml`, so it need to be unique.

**How could we build a query and ensure that the result is unique?**

Response:
- We use `createNamedQuery(java.lang.String name, java.lang.Class<T> resultClass)` to build a query. the use `resultClass` to indicate the type of query result.

## Question 3
**Add a screencopy of this page to your report, with the URL**

Response:
- screenshot
<img src="./figs/items.png" alt="screenshot" style="display:block; max-width:500px;">

- URL: http://localhost:8080/prwebSpr/listitems.do

**When we created the EntityManagerFactory in ItemManagerImpl, we give a parameter. Where does it come from?**

Response:
- The parameter is the name of persistent unit that we named in `persistence.xml`

**In the page() method, we use a named query, where is it defined?**

Response:
- It was defined in annotation `@NamedQuery` of  `Item` class.

**When we display the Item, in the JSP file, we access id, title, etc. How do we access them? Which other way of accessing these attributes could you use?**

Response:
- we access them as arrays

- We can also acess them as attributes like:
`item.id`, `item.title`, etc.
