# Rapport - slide 3
> Author: CAI Pengfei

## Question 1
**Add a screencopy of the 2 pages to your
report, with the URL**

Response:
- URL: http://localhost:8080/prwebSpr/index.do
<img src="./figs/login.png" alt="screenshot" style="display:block; max-width:500px;">

- URL: http://localhost:8080/prwebSpr/listitems.do
<img src="./figs/login_item.png" alt="screenshot" style="display:block; max-width:500px;">

**The method that handles POST does not have the same parameters than the one that Handles GET but it still works. Why?**

Response:
- Because SPRING use Design Pattern, the import thing is annotation, we can use other function name and other number of parameters that we want.

**What is the ModelAttribute annotation used for?**

Response:
- `@ModelAttribute` is an annotation that binds a method parameter or method return value to a named model attribute. For our project, it has binds `login` and `pwd` in jsp file to `User` model attributes.

## Question 2
**Add screencopies of the result to your report
What is the URL called when you click on the button in itemAdd.jsp? Is it called with method GET or POST?**

Response:
- URL: http://localhost:8080/prwebSpr/index.do
<img src="./figs/login.png" alt="screenshot" style="display:block; max-width:500px;">

- URL: http://localhost:8080/prwebSpr/listitems.do
<img src="./figs/login_item.png" alt="screenshot" style="display:block; max-width:500px;">

- URL: http://localhost:8080/prwebSpr/itemsAdd.do
<img src="./figs/item_add.png" alt="screenshot" style="display:block; max-width:500px;">

- When I click the button in `itemAdd.jsp`, URL: http://localhost:8080/prwebSpr/itemCreate.do will be called and with method `POST`.

**Which route did you define in dispatcher-servlet.xml? Which controller do you call?**

Response:
- `<prop key="itemsAdd.do">itemAddController</prop>`

- I call the `itemAddController`

**Which view is called in ItemAddController.java with the GET request? With the POST request?**

Response:
- `index.jsp` will be called with the GET request.

- `itemAdd.jsp` will be called with the POST method.

## Question 3
**Insert a new item, with you as a seller.
Add a screencopy of the pages to your
report, with the URL.**

- **List before adding an item**

- **Adding an item**

- **List after adding an item**

Response:
- Before add a new Item - URL: http://localhost:8080/prwebSpr/listitems.do
<img src="./figs/login_item.png" alt="screenshot" style="display:block; max-width:500px;">

- adding an item - URL: http://localhost:8080/prwebSpr/itemsAdd.do
<img src="./figs/item_adding.png" alt="screenshot" style="display:block; max-width:500px;">

- After add a new Item - URL: http://localhost:8080/prwebSpr/index.do
<img src="./figs/item_added.png" alt="screenshot" style="display:block; max-width:500px;">

**Why did we use the EntityManager to save
the item instead of a SQL request?**

Response:
- Certain projects require engineers to focus more on the object model rather than on the actual SQL queries used to access data stores. With JPA (implementations) or most other ORM frameworks, it is possible to have all entities i.e. tables in your database, modelled as classes in Java. Additionally, it also possible to embed behavior into these classes and therefore achieve a behaviorally rich domain model.
