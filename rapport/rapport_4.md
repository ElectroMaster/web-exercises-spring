# Rapport -slide 4
> Author: CAI Pengfei

## Question 1
**Add a screencopy of your page to your report, with the URL**

Response:
- URL: http://localhost:8080/prwebSpr/listitems.do
<img src="./figs/item_category.png" alt="screenshot" style="display:block; max-width:500px;">

**Explain the meaning of ${item['categoryId']['name']}
Which syntax could you use for the same result?**

Response:
- we access item as arrays

- We can also acess it as attributes like:
`item.categoryId.name`

## Question 2
**Try your program without changing anything else to the ItemManager, ItemManagerImpl and Item classes. Add a copy of the inserted item page to your report**

**What is the result? Explain it.**

Response:
- When adding an item. URL: http://localhost:8080/prwebSpr/itemsAdd.do
<img src="./figs/item_adding_category.png" alt="screenshot" style="display:block; max-width:500px;">

- After adding an item, URL: http://localhost:8080/prwebSpr/itemCreate.do
<img src="./figs/item_added_category.png" alt="screenshot" style="display:block; max-width:500px;">

- Explaination: we obtain the list of possible categories by using persistence query `category.findAll` and transfer the result list to Object `listCategory`. Then we display the `listCategory` by using element `<select></select>` and jstl. After we've populated the line which need to be added, the result will be parsed to a class Item by using annotation `@ModelAttribute`. Then we commit it to database and we call `item.jsp` to display all rows of table item.
