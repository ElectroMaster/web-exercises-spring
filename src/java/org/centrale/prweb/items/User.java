/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.prweb.items;

/**
 *
 * @author exia
 */
public class User {
    private String login;
    private String password;

    public String getLogin() {
        return login;
    }

    public String getPwd() {
        return password;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPwd(String pwd) {
        this.password= pwd;
    }
    
    
}
