/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.prweb.controller;

import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.prweb.items.Category;
import org.centrale.prweb.manager.ItemManager;
import org.centrale.prweb.manager.ItemManagerImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author exia
 */
@Controller
public class ItemAddController {
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView handleGET(HttpServletRequest request,
            HttpServletResponse response){
//        TODO:
        ModelAndView result = new ModelAndView("index");
        return result;
    }
    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView handlePOST(HttpServletRequest request,
            HttpServletResponse response){
        ItemManager theItemManager = ItemManagerImpl.getInstance();
        Collection<Category> categories = theItemManager.listCategories();
        
        
        ModelAndView result = new ModelAndView("itemAdd");
        result.addObject("listCategory", categories);
        return result;
    
    }
}