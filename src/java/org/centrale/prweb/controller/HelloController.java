/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.centrale.prweb.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.centrale.prweb.items.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author exia
 */
@Controller
public class HelloController {
    public HelloController(){
        
    }
    
    @RequestMapping(method=RequestMethod.GET)
    public ModelAndView handleHello(HttpServletRequest request,
            HttpServletResponse response){
        ModelAndView result = new ModelAndView("hello");
        User anUser = new User();
        anUser.setLogin("admin");
        anUser.setPwd("123456");
        
        result.addObject("anUser", anUser);
        return result;
    }
}
