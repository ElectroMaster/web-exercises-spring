<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <spring:url value="/css/login.css" var="login_css"/>
        <link rel="stylesheet" href="${login_css}">
        <title>Connection page</title>
    </head>

    <body>
        <form:form action="listitems.do" method="POST">
            <p><c:out value ="${errorMsg}"/> </p>
            <h1>Auction Login</h1>
            <p><input type="text" name="login"></p>
            <p><input type="password" name="pwd"></p>
            <p><button type="submit">Login</button></p>
        </form:form>
            
            
    </body>
</html>
